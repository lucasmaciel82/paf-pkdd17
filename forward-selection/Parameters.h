// Copyright 2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Loïc Cerf (magicbanana@gmail.com)

// This file is part of forward-selection.

// forward-selection is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// forward-selection is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with forward-selection; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#ifndef PARAMETERS_H_
#define PARAMETERS_H_

// Output
/* OUTPUT turns on the output. This option may be disabled to evaluate the performance of forward-selection independently from the writing performances of the disk. */
#define OUTPUT

/* DEBUG turns on the output (on the standard output) of information during the selection of the patterns. This option may be enabled by who wishes to understand how this selection is performed on a small number of error-tolerant patterns. */
//#define DEBUG

#endif /*PARAMETERS_H_*/
