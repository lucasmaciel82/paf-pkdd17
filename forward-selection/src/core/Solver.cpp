#include "Solver.h"

DataTrie<double> Solver::alldata(0.00);

Solver::Solver(const Pattern &allpatterns){
	vector<vector<unsigned int>> tuples = allpatterns.toTuples();

	total_size = allpatterns.getArea();
	total_data = total_square_data = 0;

	for (const auto& tuple: tuples){
		double vlr = alldata.getTuple(tuple.begin(), tuple.end());
		if (vlr == 0) continue;

		if (vlr == 2){
			alldata.addTuple(tuple.begin(), tuple.end(), 0);
			continue;
		}

		total_square_data += vlr*vlr;
		total_data += vlr;
	}

	#ifdef DEBUG
	cout << "total_data: " << total_data << " total_size: " << total_size << endl;
	#endif
}

Solver::~Solver(){}

vector<Pattern*> Solver::select(vector<Pattern*>& patterns){
	priority_queue<SelectionNode*, vector<SelectionNode*>, SelectionNode::Comparator> q;

    SelectionNode::setDataParameters(total_data, total_size);
    SelectionNode::setTotalVariables(total_size);

    double total_res = total_square_data - total_data * total_data / total_size;
    double total_metric = INT_MAX;

    for (auto& pat : patterns){
        SelectionNode* s = new SelectionNode(pat);
        s->setResidual(total_res);
        q.push(s);
    }

    vector<Pattern*> answer;

    while (!q.empty()){
        SelectionNode* actual = q.top();
        q.pop();

#ifdef DEBUG
        cout << "Get node " << actual << ": " << *actual->getKey() << endl << "   coef: " << actual->getCoeff() << endl;
		cout << "density: " << actual->getKey()->getMembershipSum() / actual->getKey()->getArea() << endl;
#endif

        if (actual->last_id == answer.size()){

            if (cmpf(actual->getCoeff(), total_metric) > 0){
                delete actual;
                continue;
            }

#ifdef DEBUG
        cout << "pattern chosen!" << endl;
#endif

            total_res = actual->getResidual();
            total_metric = actual->getCoeff();
            answer.push_back(actual->getKey());
            SelectionNode::incVariables();
            SelectionNode::add(actual);
            delete actual;
        }
        else{
            actual->last_id = answer.size();
            actual->setResidual(total_res);
            q.push(actual);
        }
    }

	return answer;
}
