#include "IO.h"
#include "DataTrie.h"
#include "Solver.h"
#include "../../Parameters.h"

using namespace std;

int main(int argc, char* argv[]){

	IO io(argc, argv);

	if (io.fail() != EX_OK){
		return io.fail();
	}

	vector<Pattern*> patterns;

	try{
		#ifdef DEBUG
			cout.precision(10);
			cout << "* READING PATTERNS" << endl;
		#endif

		patterns = io.readNSets();
	}
	catch (IncorrectNbOfDimensionsException e){
		cerr << e.what();
		return EX_DATAERR;
	}

	vector<vector<unsigned int>> tmp(io.getSizes());
	Pattern all_sets(tmp);

	for (const auto& p : patterns){
		all_sets = all_sets + *p;
	}

	vector< vector<unsigned int> > tuples = all_sets.toTuples();
	for (const auto& tuple : tuples){
		Solver::alldata.addTuple(tuple.begin(), tuple.end(), 2.0);
	}

	try{
		#ifdef DEBUG
			cout << "* READING TUPLES" << endl;
		#endif

		io.readTuples(Solver::alldata);
	}
	catch (IncorrectNbOfDimensionsException e){
		cerr << e.what();
		return EX_DATAERR;
	}

	Solver solver(all_sets);

	for (int i = 0; i < patterns.size(); i++){
		vector<vector<unsigned int>> aux = patterns[i]->toTuples();
		patterns[i]->setMembershipSum(Solver::alldata.sumSet(aux));
	}

#ifdef DEBUG
	cout << "* RUNNING SOLVER" << endl;
#endif

	vector<Pattern*> hidden_patterns = solver.select(patterns);
	io.writePatterns(hidden_patterns);

	for (Pattern *p : patterns)
		delete p;

	return EX_OK;
}
