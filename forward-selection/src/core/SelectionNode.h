#ifndef SELECTION_NODE_H_
#define SELECTION_NODE_H_

#include "util.h"
#include "DataTrie.h"
#include "Pattern.h"

class SelectionNode{
public:
	SelectionNode(Pattern* const p);

	Pattern* getKey();

	//Static functions for coefficient of determination
	static void setMode(int v);
	static void setDataParameters(const double total_sum, const unsigned int total_area);
	static void setTotalVariables(const unsigned int totalVariablesParam);
	static void incVariables();
	static void add(SelectionNode* s);

	//The coefficients
	double getDiff() const;
	double getCoeff() const;
	void setCoeff(const double acc);
	double getResidual() const;
	void setResidual(const double acc);

	//public last_id
	unsigned int last_id;

	class Comparator{
	public:
		Comparator(){}
		bool operator() (const SelectionNode* a, const SelectionNode* b){
			return a->getCoeff() > b->getCoeff() && fabs(a->getCoeff() - b->getCoeff()) > 1e-10;
		}
	};

protected:

	static int mode;

	/* To compute the coefficients for selection nodes */
	static DataTrie<double> data;
	static unsigned int totalVariables;
	static unsigned int currentSelectedVariables;
	double coeff, res, diffres;

	// properties of the pattern node
	Pattern *key;

	// Convert to a list of tuples
	double getSSres() const;

};

#endif  /* SELECTION_NODE_H_ */
