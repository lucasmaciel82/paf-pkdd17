#!/bin/bash
while read line; do
    echo $line;
    echo ${line%/*};
    cd $line;
    for cm in quality size time; do
        gnuplot $cm.plt
        mv ../OUTPUT.eps ../$cm"-"${line%/*}.eps
    done;
    cd ..;
done < /dev/stdin
