set logscale x

#set xlabel "nb of correct observations"
#set ylabel "memory"
set xrange [16:1]
set xtics 1,2
#set yrange [0:1.5]
#set ytics (0, 0.2, 0.4, 0.6, 0.8, 1, "" 1.2, "" 1.4)
#set datafile missing ""
set grid
set terminal postscript eps enhanced lw 3 "Times-Roman" 24
set output "../OUTPUT.eps"
set nokey
plot \
"mdh+paf0" using 1:6 with linespoints pt 5 ps 1.7 title "PAF",\
"mdh+paf1" using 1:6 with linespoints pt 7 ps 1.7 title "APAF"
