#!/usr/bin/python3
# coding=utf-8
import sys
import numpy as np
import pandas as pd
from src.utils import sh

# def sc(df,s1,s2 = ''):
#     return df.filter(regex='%s(.*)%s$' % (s1, s2)).columns[0]

class ExtDF(pd.DataFrame):
    def rename(self, index=None, columns=None, **kwargs):
        newcolumns = {k if (k in self.keys()) else self.filter(regex='%s' % k).columns[0]: v for k, v in columns.items()}
        return super(ExtDF, self).rename(index=index, columns=newcolumns, **kwargs)

    # def rename(self, columns, inplace):
    #     super().rename(columns, inplace)

def plot(dim, epsilon_relative, minsizes, basefolder, csvfile):
    # cria pasta
    folder = "%sdim_%serel_%smin" % (dim, epsilon_relative, minsizes)
    path = "%s/%s" % (basefolder, folder)
    sh("mkdir -p %s" % path)

    # converte parametros
    df = pd.read_csv(csvfile)
    df = df.loc[df['_dim'] == int(dim)]
    df = df.loc[df['_minsizes'] == int(minsizes)]
    df = df.loc[df['_epsilon_relative'] == float(epsilon_relative)]

    df = df[df['_coeff'] == 2]

    # altera nomes
    df = df.rename(columns={'_correct_obs': 'alpha'})

    #limpa não-definidos
    df = df.replace('not defined', 0)
    df['time_tribox_user'] = df['time_tribox_user'].astype('float64')

    # soma os tempos
    df['time_mdh']          = df['time_mdh_user']
    df['time_mdh-paf0']     = df['time_mdh-paf0_user'] + df['time_mdh']
    df['time_mdh-paf1']     = df['time_mdh-paf1_user'] + df['time_mdh']
    df['time_tribox']       = df['time_tribox_user'] + df['time_mdh']
    df['time_tribox-paf1']  = df['time_tribox-paf1_user'] + df['time_tribox']

    # agrupa os dados nas observacoes corretas e tira a média dos dados
    mdh =         ExtDF( df[['alpha', 'corr_mdh', 'quant_mdh', 'time_mdh']]                         .groupby('alpha').agg(np.mean))
    mdh_paf0 =    ExtDF( df[['alpha', 'corr_mdh-paf0', 'quant_mdh-paf0', 'time_mdh-paf0', 'mem_mdh-paf0_average_resident_size','mem_mdh-paf0_maximum_resident_size']].groupby('alpha').agg(np.mean))
    mdh_paf1 =    ExtDF( df[['alpha', 'corr_mdh-paf1', 'quant_mdh-paf1', 'time_mdh-paf1', 'mem_mdh-paf1_average_resident_size','mem_mdh-paf1_maximum_resident_size']]          .groupby('alpha').agg(np.mean))
    tribox =      ExtDF( df[['alpha', 'corr_tribox', 'quant_tribox', 'time_tribox']]                .groupby('alpha').agg(np.mean))
    tribox_paf1 = ExtDF( df[['alpha', 'corr_tribox-paf1', 'quant_tribox-paf1', 'time_tribox-paf1']] .groupby('alpha').agg(np.mean))

    # renomeia as colunas
    for p in [mdh, mdh_paf0, mdh_paf1, tribox, tribox_paf1]:
        p.rename(columns={'corr': 'quality', 'quant': 'size', 'time': 'time'}, inplace=True)
    mdh_paf0.rename(columns={'average_resident_size': 'avg-mem', 'maximum_resident_size': 'max-mem'}, inplace=True)
    mdh_paf1.rename(columns={'average_resident_size': 'avg-mem', 'maximum_resident_size': 'max-mem'}, inplace=True)

    # remove pontos com erro
    for status,status_error,data in [
        ('status_mdh', 'status_mdh-error', mdh),
        ('status_mdh-paf0', 'status_mdh-paf0-error', mdh_paf0),
        ('status_mdh-paf1', 'status_mdh-paf1-error', mdh_paf1),
        ('status_tribox', 'status_tribox-error', tribox),
        ('status_tribox-paf1', 'status_tribox-paf1-error', tribox_paf1)
    ]:
        df[status].fillna('', inplace=True)
        df[status_error].fillna('', inplace=True)

        alphas = df[df[status] == 'error']['alpha'].drop_duplicates().values
        if alphas.size > 0:
            data.ix[alphas] = 'NaN'

        alphas = df[df[status_error] == 'err_tempo']['alpha'].drop_duplicates().values
        if alphas.size > 0:
            data.ix[alphas] = 'NaN'

        alphas = df[df[status_error] == 'err_memoria']['alpha'].drop_duplicates().values
        if alphas.size > 0:
            data.ix[alphas] = 'NaN'

        alphas = df[df[status_error] == 'err_configuracao']['alpha'].drop_duplicates().values
        if alphas.size > 0:
            data.ix[alphas] = 'NaN'

    for p in [mdh, mdh_paf0, mdh_paf1, tribox, tribox_paf1]:

        # quando estoura tempo não deve apresentar o número de padrões e o tempo
        # for i in p.index:
        #     if isinstance(p.loc[i, 'time'], float) and p.loc[i, 'time'] > 3600:
        #         p.loc[i, ['size', 'time']] = "NaN"

        # preenche valores nulos
        p.fillna("NaN", inplace=True)

    # quando o mdh não trazer nenhum padrão deve remover os pontos gerados pelos outros algoritmos que dependem dele
    for i in mdh.index:
        if mdh.loc[i, 'size'] == 0:
            mdh_paf0.loc[i, ['size', 'quality', 'time']] = "NaN"
            mdh_paf1.loc[i, ['size', 'quality', 'time']] = "NaN"

    # cria os arquivos separados por espaco
    mdh.to_csv('%s/mdh' % path, sep=' ')
    mdh_paf0.to_csv('%s/mdh+paf0' % path, sep=' ')
    mdh_paf1.to_csv('%s/mdh+paf1' % path, sep=' ')
    tribox.to_csv('%s/tribox' % path, sep=' ')
    tribox_paf1.to_csv('%s/tribox+paf1' % path, sep=' ')

    # comenta a primeira linha de cada um
    for s in ['mdh', 'mdh+paf0', 'mdh+paf1', 'tribox', 'tribox+paf1']:
        sh("""sed -i '' '1 s/^/#/' %s/%s""" % (path, s))

    # cria quality.plt
    quality_filename = "quality.plt"
    quality_fullpath = "%s/%s" % (path, quality_filename)
    sh("cp ./templates/quality.plt %s" % quality_fullpath)
    sh("""sed -i '' 's/OUTPUT/%s-%s/g' %s""" % ("quality", folder, quality_fullpath))

    # cria size.plt
    size_filename = "size.plt"
    size_fullpath = "%s/%s" % (path, size_filename)
    sh("cp ./templates/size.plt %s" % size_fullpath)
    sh("""sed -i '' 's/OUTPUT/%s-%s/g' %s""" % ("size", folder, size_fullpath))
    sh("""sed -i '' 's/SIZE_HIDDEN_PATTERNS/%s/g' %s""" % ('4.0', size_fullpath))

    # cria time.plt
    time_filename = "time.plt"
    time_fullpath = "%s/%s" % (path, time_filename)
    sh("cp ./templates/time.plt %s" % time_fullpath)
    sh("""sed -i '' 's/OUTPUT/%s-%s/g' %s""" % ("time", folder, time_fullpath))

    # cria memory.plt
    time_filename = "memory.plt"
    time_fullpath = "%s/%s" % (path, time_filename)
    sh("cp ./templates/memory.plt %s" % time_fullpath)
    sh("""sed -i '' 's/OUTPUT/%s-%s/g' %s""" % ("memory", folder, time_fullpath))

if __name__ == "__main__":
    if len(sys.argv) == 6:
        d = sys.argv[1]
        erel = sys.argv[2]
        s = sys.argv[3]
        folder = sys.argv[4]   # './res'
        csv = sys.argv[5]
        plot(d, erel, s, folder, csv)
    else:
        print("Usage <dim> <epsilon_relative> <minsizes> <basefolder> <csvfile>")
        exit()