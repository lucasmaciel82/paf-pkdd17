#!/bin/bash

find $1 -type f -name "*.lines" -exec cat {} \; | awk 'BEGIN {FS = " "}; {print "./create_lines.sh " $1 " > output_from_lines/" $2 }' > exec.sh

chmod +x exec.sh
./exec.sh
rm exec.sh
