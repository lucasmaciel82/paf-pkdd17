// Copyright 2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of paf.

// paf is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// paf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with paf; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#ifndef TRIE_H_
#define TRIE_H_

#include "DenseCrispTube.h"
#include "DenseFuzzyTube.h"
#include "SparseCrispTube.h"
#include "SparseFuzzyTube.h"

class Trie final : public AbstractData
{
 public:
  Trie();
  Trie(const Trie& otherTrie);
  Trie(Trie&& otherTrie);
  Trie(const vector<unsigned int>::const_iterator cardinalityIt, const vector<unsigned int>::const_iterator cardinalityEnd, const float densityThreshold);

  ~Trie();

  Trie& operator=(const Trie& otherTrie);
  Trie& operator=(Trie&& otherTrie);
  friend ostream& operator<<(ostream& out, const Trie& trie);

  void print(vector<unsigned int>& prefix, ostream& out) const;
  void setTuples(const vector<vector<unsigned int>>& tuples, const float membership);
  const bool setTuples(const vector<vector<unsigned int>>::const_iterator dimensionIt, const float noise);
  void turnCrisp();
  void sortTubes();
  
  const float noiseSum(const vector<vector<unsigned int>>& nSet) const;
  const float noiseSum(const vector<vector<unsigned int>>::const_iterator dimensionIt) const;

 protected:
  vector<AbstractData*> hyperplanes;

  Trie(const vector<unsigned int>::const_iterator cardinalityIt, const vector<unsigned int>::const_iterator cardinalityEnd);

  void init(const vector<unsigned int>::const_iterator cardinalityIt, const vector<unsigned int>::const_iterator cardinalityEnd);  
  
  Trie* clone() const;
  void copy(const Trie& otherTrie);
};

#endif /*TRIE_H_*/
