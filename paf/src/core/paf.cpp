// Copyright 2016 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of paf.

// paf is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// paf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with paf; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#include <iostream>
#include <boost/program_options.hpp>
#include "sysexits.h"

#include "Dendrogram.h"

using namespace boost::program_options;

int main(int argc, char* argv[]){

    Dendrogram solver;

    double lambda_0;
    float shift;
    bool select_top_dendrogram = false;

    string outputDimensionSeparator;
    string outputElementSeparator;
    bool isSizePrinted;
    string patternSizeSeparator;
    string sizeSeparator;
    bool isAreaPrinted;
    string sizeAreaSeparator;
    // Parsing the command line and the option file

    try
    {
        string optionFileName;
        double maximalNbOfCandidates = 0;

        options_description generic("Generic options");
        generic.add_options()
            ("help,h", "produce help message")
            ("version,V", "display version information and exit")
            ("opt", value<string>(&optionFileName), "set the option file name (by default [pattern-file].opt if present)");

        options_description mandatory("Mandatory options (on the command line or in the option file)");
        mandatory.add_options()
            ("file,f", value<string>(), "set data file")
            ("memory,m", value<double>(&maximalNbOfCandidates), "hierarchically agglomerate the input patterns considering at most as many candidates as given in argument in million");

        options_description basicConfig("Basic configuration (on the command line or in the option file)");
        basicConfig.add_options()
            ("shift", value<float>(&shift)->default_value(0), "set similarity shift")
            ("intercept", value<double>(&lambda_0)->default_value(-1), "set intercept (default intercept is the pattern density)")
            ("top-dendrogram,top", "Set to output the nodes of dendrogram as a tree.")
            ("density,d", value<float>()->default_value(0), "set threshold between 0 (completely dense storage) and 1 (minimization of memory usage) to trigger a dense storage of the data (by default 0)")
            ("out,o", value<string>(), "set output file name (by default [data-file].out)");

        options_description io("Input/Output format (on the command line or in the option file)");
        io.add_options()
            ("ids", value<string>()->default_value(" "), "set any character separating two dimensions in input data")
            ("ies", value<string>()->default_value(","), "set any character separating two elements in input data")
            ("ods", value<string>(&outputDimensionSeparator)->default_value(" "), "set string separating two dimensions in output data")
            ("oes", value<string>(&outputElementSeparator)->default_value(","), "set string separating two elements in output data")
            ("ps", "print sizes in output data")
            ("css", value<string>(&patternSizeSeparator)->default_value(" : "), "set string separating agglomerates from sizes in output data")
            ("ss", value<string>(&sizeSeparator)->default_value(" "), "set string separating sizes of the different dimensions in output data")
            ("pa", "print areas in output data")
            ("sas", value<string>(&sizeAreaSeparator)->default_value(" : "), "set string separating sizes from areas in output data");

        options_description hidden("Hidden options");
        hidden.add_options()
            ("pattern-file", value<string>(), "set pattern file");

        positional_options_description p;
        p.add("pattern-file", -1);

        options_description commandLineOptions;
        commandLineOptions.add(generic).add(mandatory).add(basicConfig).add(io).add(hidden);

        variables_map vm;
        store(command_line_parser(argc, argv).options(commandLineOptions).positional(p).run(), vm);
        notify(vm);

        if (vm.count("help")){
            cout << "Usage: paf [options] pattern-file" << endl << generic << mandatory << basicConfig << io;
            return EX_OK;
        }

        if (vm.count("version")){
            cout << "paf version 0.1.6" << endl;
            return EX_OK;
        }

        ifstream optionFile;
        if (vm.count("opt")){
            optionFile.open(optionFileName.c_str());
            if (!optionFile)
                throw NoFileException(optionFileName.c_str());
            optionFile.close();
        }
        else{
            if (!vm.count("pattern-file")){
                cerr << "Usage: paf [options] pattern-file" << endl << generic << mandatory << basicConfig << io;
                return EX_USAGE;
            }
            optionFileName = vm["pattern-file"].as<string>() + ".opt";
        }

        options_description config;
        config.add(mandatory).add(basicConfig).add(io).add(hidden);
        optionFile.open(optionFileName.c_str());
        store(parse_config_file(optionFile, config), vm);
        notify(vm);
        optionFile.close();

        if (!(vm.count("pattern-file"))){
            cerr << "Usage: paf [options] pattern-file" << endl << generic << mandatory << basicConfig << io;
            return EX_USAGE;
        }

        if (!vm.count("file")){
            throw UsageException("file option is mandatory!");
        }

        if (vm.count("ha")){
            if (maximalNbOfCandidates <= 0){
                throw UsageException("ha option should provide a strictly positive double!");
            }
            if (maximalNbOfCandidates > numeric_limits<unsigned int>::max()){
                throw UsageException("ha option should provide a smaller number of million of candidates!");
            }
        }

        if (vm.count("top-dendrogram")){
        	select_top_dendrogram = true;
        }

        if (vm["density"].as<float>() < 0 || vm["density"].as<float>() > 1){
            throw UsageException("density option should provide a float between 0 and 1!");
        }

        if (vm.count("out")){
            solver.parse(vm["pattern-file"].as<string>().c_str(), vm["file"].as<string>().c_str(), maximalNbOfCandidates, vm["ids"].as<string>().c_str(), vm["ies"].as<string>().c_str(), vm["density"].as<float>(), vm["out"].as<string>().c_str());
        }
        else{
            solver.parse(vm["pattern-file"].as<string>().c_str(), vm["file"].as<string>().c_str(), maximalNbOfCandidates, vm["ids"].as<string>().c_str(), vm["ies"].as<string>().c_str(), vm["density"].as<float>(), (vm["pattern-file"].as<string>() + ".out").c_str());
        }
        isSizePrinted = vm.count("ps");
        isAreaPrinted = vm.count("pa");
    }
    catch (unknown_option& e){
        cerr << "Unknown option!" << endl;
        return EX_USAGE;
    }
    catch (UsageException& e){
        cerr << e.what() << endl;
        return EX_USAGE;
    }
    catch (NoFileException& e){
        cerr << e.what() << endl;
        return EX_IOERR;
    }
    catch (DataFormatException& e){
        cerr << e.what() << endl;
        return EX_DATAERR;
    }

    solver.paf(shift, lambda_0, select_top_dendrogram, outputDimensionSeparator.c_str(), outputElementSeparator.c_str(), patternSizeSeparator.c_str(), sizeSeparator.c_str(), sizeAreaSeparator.c_str(), isSizePrinted, isAreaPrinted);
    return EX_OK;
}
