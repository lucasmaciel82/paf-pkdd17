// Copyright 2010,2011,2012,2013,2014,2015,2016 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of paf.

// paf is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// paf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with paf; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#ifndef DENSE_CRISP_TUBE_H_
#define DENSE_CRISP_TUBE_H_

#include <boost/dynamic_bitset.hpp>

#include "AbstractData.h"

using namespace boost;

class DenseCrispTube final : public AbstractData
{
 public:
  DenseCrispTube(const vector<float>& tube);
  DenseCrispTube(const vector<pair<unsigned int, float>>& tube);
    
  void print(vector<unsigned int>& prefix, ostream& out) const;

  const float noiseSum(const vector<vector<unsigned int>>::const_iterator dimensionIt) const;

 protected:
  dynamic_bitset<> tube;

  DenseCrispTube* clone() const;
};

#endif /*DENSE_CRISP_TUBE_H_*/
