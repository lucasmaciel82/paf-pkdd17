// Copyright 2015,2016 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of paf.

// paf is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// paf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with paf; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#ifndef DENDROGRAM_NODE_H_
#define DENDROGRAM_NODE_H_

#include <unordered_set>
#include <unordered_map>
#include <queue>

#include "../utilities/UsageException.h"
#include "../utilities/vector_hash.h"
#include "CandidateNode.h"

#if defined DEBUG || defined TIME || defined DETAILED_TIME
#include <iostream>
#if defined TIME || defined DETAILED_TIME
#include <chrono>

using namespace std::chrono;
#endif
#endif

class CandidateNode;

class Dendrogram;

class DendrogramNode{
public:
	DendrogramNode(const vector<vector<unsigned int>>& nSetParam);
	DendrogramNode(const CandidateNode* candidate);

	friend ostream& operator<<(ostream& out, const DendrogramNode& patternNode);

	void completeLeaf();      /* to call on every leaf after external2InternalDimensionOrder and data are defined */
	void addChild(DendrogramNode* new_child);
	void setParent(DendrogramNode* new_parent);

	vector<vector<unsigned int>> unionWith(const DendrogramNode& otherPatternNode) const;
	const vector<DendrogramNode*>& getChildren() const;
	DendrogramNode* getParent();

	vector<vector<unsigned int>>& getNSet();
	unsigned int getArea() const;
	double getMembershipSum() const;

	bool isSuperSet(DendrogramNode *node) const;

	CandidateNode* topCandidate();
	set<CandidateNode*, CandidateNode::Comparator>& getCandidates();
	unordered_set<CandidateNode*>& getPartners();
	bool addCandidate(CandidateNode* new_candidate, unsigned int lim);
	void addPartner(CandidateNode* partner);
	void removeCandidate(CandidateNode* candidate);
	int nbOfCandidates() const;

	//Static functions for output

	static void setSeparator(const char* outputDimensionSeparatorParam, const char* outputElementSeparatorParam, const char* patternSizeSeparatorParam, const char* sizeSeparatorParam, const char* sizeAreaSeparatorParam);
	static void setPrinted(bool isSizePrintedParam, bool isAreaPrintedParam);

	static void setExternal2InternalDimensionOrder(vector<unsigned int>& external2InternalDimensionOrderParam);
	static void setIds2Labels(vector<vector<string>>& ids2LabelsParam);

protected:


	vector<vector<unsigned int>> nSet;
	unsigned int area;
	double membershipSum;

	vector<DendrogramNode*> children;
	DendrogramNode* parent;
	set<CandidateNode*, CandidateNode::Comparator> candidates;
	unordered_set<CandidateNode*> partners;

	static vector<unsigned int> external2InternalDimensionOrder;
	static vector<vector<string>> ids2Labels;

	static string outputDimensionSeparator;
	static string outputElementSeparator;
	static string patternSizeSeparator;
	static string sizeSeparator;
	static string sizeAreaSeparator;
	static bool isSizePrinted;
	static bool isAreaPrinted;

};

#endif  /* DENDROGRAM_NODE_H_ */
