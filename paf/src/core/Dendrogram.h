#ifndef DENDROGRAM_H_
#define DENDROGRAM_H_

#include <unordered_map>
#include <queue>

#include "../utilities/UsageException.h"
#include "../utilities/vector_hash.h"
#include "PatternFileReader.h"
#include "NoisyTupleFileReader.h"
#include "CandidateNode.h"
#include "DendrogramNode.h"
#include "DataTrie.h"
#include "../../Parameters.h"

#if defined DEBUG || defined TIME || defined DETAILED_TIME
#include <iostream>
#if defined TIME || defined DETAILED_TIME
#include <chrono>

using namespace std::chrono;
#endif
#endif

class Dendrogram{
public:
	Dendrogram();
	~Dendrogram();

	static int cmpf(double a, double b = 0);

	void parse(const char* patternFileName, const char* dataFileName, const double maximalNbOfCandidates, const char* inputDimensionSeparator, const char* inputElementSeparator, const float densityThreshold, const char* outputFileName);
	void paf(const float shift, const double lambda_0, const bool top_dendrogram, const char* outputDimensionSeparator, const char* outputElementSeparator, const char* patternSizeSeparator, const char* sizeSeparator, const char* sizeAreaSeparator, const bool isSizePrinted, const bool isAreaPrinted);

	static Trie data;
	static ofstream outputFile;

protected:

	double total_data, total_square_data;
	unsigned int total_size;

	unsigned int nbOfParents;
	list<DendrogramNode*> dendrogramNodes;
	list<DendrogramNode*> dendrogramFrontier;
	set<CandidateNode*, CandidateNode::Comparator> bagOfCandidates;
	unordered_map<DendrogramNode*, unordered_map<DendrogramNode*, bool>> mark;

	void fillCandidatesList(list<DendrogramNode*>::iterator curr);
	void agglomerate();

	static void print(DendrogramNode *node, string backspace, bool isLastBrother, bool isRoot = false);

#if defined TIME || defined DETAILED_TIME
	static steady_clock::time_point overallBeginning;
#endif
#ifdef DETAILED_TIME
	static double parsingDuration;
#endif
};

#endif  /* DENDROGRAM_H_ */
