// Copyright 2015,2016 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of paf.

// paf is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// paf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with paf; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#ifndef CANDIDATE_NODE_H_
#define CANDIDATE_NODE_H_

#include <list>
#include <unordered_set>
#include <iostream>
using namespace std;

#include "Trie.h"

class DendrogramNode;
class Dendrogram;

class CandidateNode{
    public:
        CandidateNode(const list<DendrogramNode*>::iterator& left, const list<DendrogramNode*>::iterator& right);
        ~CandidateNode();

        const vector<vector<unsigned int>>& getNSet() const;
        const unsigned int getArea() const;
        const double getMembershipSum() const;
        void computeExactlyQuadraticErrorVariation();
        void setQuadraticErrorVariation(const double twoPatternModelG);

        const double getQuadraticErrorVariation() const;

        list<DendrogramNode*>::iterator getLeftChild() const;
        list<DendrogramNode*>::iterator getRightChild() const;

        static void setLambda0(const double param_lambda_0);
        static void setSimilarityShift(const double similarityShift);
        static const double getOneMinusSimilarityShift();
        static const bool smallerQuadraticErrorVariation(const CandidateNode* node1, const CandidateNode* node2);

        class Comparator{
        public:
            Comparator(){}
            bool operator()(CandidateNode* const a, CandidateNode* const b){
                if (a->getQuadraticErrorVariation() == b->getQuadraticErrorVariation()){
                    return a < b;
                }
                return a->getQuadraticErrorVariation() < b->getQuadraticErrorVariation();
            }
        };

    protected:
        vector<vector<unsigned int>> nSet;
        double membershipSum;
        double quadraticErrorVariation;
        unsigned int area;

        list<DendrogramNode*>::iterator leftChild, rightChild;

        static double lambda_0;
        static double oneMinusSimilarityShift;

        void computeQuadraticErrorVariation(const double lambda_t, const double membershipSumAtIntersection, const unsigned int intersectionArea);
};

#endif  /* CANDIDATE_NODE_H_ */
