// Copyright 2015,2016 Loïc Cerf (lcerf@dcc.ufmg.br)

// This file is part of paf.

// paf is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// paf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with paf; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#include "CandidateNode.h"
#include "DendrogramNode.h"
#include "Dendrogram.h"

double CandidateNode::oneMinusSimilarityShift;
double CandidateNode::lambda_0;

CandidateNode::CandidateNode(const list<DendrogramNode*>::iterator& left, const list<DendrogramNode*>::iterator& right){
    DendrogramNode& child1 = **left;
    DendrogramNode& child2 = **right;
    nSet = child1.unionWith(child2);

    // Compute total area
    area = 1;
    for (const auto& it : nSet)
        area *= it.size();

    // Compute the intersection of the two children and the area of this intersection
    unsigned int intersectionArea = 1;
    vector<vector<unsigned int>> intersection;
    intersection.reserve((*left)->getNSet().size());
    vector<vector<unsigned int>>::const_iterator child1DimensionIt = (*left)->getNSet().begin();
    for (const vector<unsigned int>& child2Dimension : (*right)->getNSet()){
        vector<unsigned int> intersectionDimension(child2Dimension.size());
        const unsigned int intersectionDimensionSize = set_intersection(child2Dimension.begin(), child2Dimension.end(), child1DimensionIt->begin(), child1DimensionIt->end(), intersectionDimension.begin()) - intersectionDimension.begin();
        intersectionDimension.resize(intersectionDimensionSize);
        intersection.push_back(intersectionDimension);
        intersectionArea *= intersectionDimensionSize;
        ++child1DimensionIt;
    }

    // Compute the heavy children (left) and lighter children (right)
    leftChild = left;
    rightChild = right;

    if ((*left)->getMembershipSum() / (*left)->getArea() < (*right)->getMembershipSum() / (*right)->getArea()){
        leftChild = right;
        rightChild = left;
    }

    const double lambda_left = (*leftChild)->getMembershipSum() / (*leftChild)->getArea();
    const double lambda_right = (*rightChild)->getMembershipSum() / (*rightChild)->getArea();

    double lambda_t = (*leftChild)->getArea() * (lambda_left - lambda_0);
    lambda_t += (*rightChild)->getArea() * (lambda_right - lambda_0);
    lambda_t -= intersectionArea * (lambda_left - lambda_0);
    lambda_t /= area;
    lambda_t += lambda_0;

    membershipSum = lambda_t * area;

    double membershipSumAtIntersection = lambda_left *  intersectionArea;

    computeQuadraticErrorVariation(lambda_t, membershipSumAtIntersection, intersectionArea);
}

CandidateNode::~CandidateNode(){
    (*leftChild)->removeCandidate(this);
    (*rightChild)->removeCandidate(this);
}

const vector<vector<unsigned int>>& CandidateNode::getNSet() const {
    return nSet;
}

const unsigned int CandidateNode::getArea() const {
    return area;
}

const double CandidateNode::getMembershipSum() const {
    return membershipSum;
}

const double CandidateNode::getQuadraticErrorVariation() const {
    return quadraticErrorVariation;
}

void CandidateNode::computeExactlyQuadraticErrorVariation(){
    // Compute the intersection of the two children and the area of this intersection
    unsigned int intersectionArea = 1;
    vector<vector<unsigned int>> intersection;
    intersection.reserve((*leftChild)->getNSet().size());
    vector<vector<unsigned int>>::const_iterator child1DimensionIt = (*leftChild)->getNSet().begin();
    for (const vector<unsigned int>& child2Dimension : (*rightChild)->getNSet()){
        vector<unsigned int> intersectionDimension(child2Dimension.size());
        const unsigned int intersectionDimensionSize = set_intersection(child2Dimension.begin(), child2Dimension.end(), child1DimensionIt->begin(), child1DimensionIt->end(), intersectionDimension.begin()) - intersectionDimension.begin();
        intersectionDimension.resize(intersectionDimensionSize);
        intersection.push_back(intersectionDimension);
        intersectionArea *= intersectionDimensionSize;
        ++child1DimensionIt;
    }

    // Compute the quality of the model composed of the two children
    membershipSum = area * oneMinusSimilarityShift - Dendrogram::data.noiseSum(nSet);
    const double lambda_t = membershipSum / area;
    const double membershipSumAtIntersection = oneMinusSimilarityShift * intersectionArea - Dendrogram::data.noiseSum(intersection);


    computeQuadraticErrorVariation(lambda_t, membershipSumAtIntersection, intersectionArea);
}

void CandidateNode::computeQuadraticErrorVariation(const double lambda_t, const double membershipSumAtIntersection, const unsigned int intersectionArea) {

    const double lambda_left = (*leftChild)->getMembershipSum() / (*leftChild)->getArea();
    const double lambda_right = (*rightChild)->getMembershipSum() / (*rightChild)->getArea();

    double variationLeft = (*leftChild)->getArea() * (lambda_t * lambda_t + lambda_left * lambda_left - 2 * lambda_left * lambda_t);
    double variationRight = (*rightChild)->getArea() * (lambda_t * lambda_t + lambda_right * lambda_right - 2 * lambda_right * lambda_t);
    variationRight -= intersectionArea * (lambda_t * lambda_t - lambda_right * lambda_right);
    variationRight -= 2 * membershipSumAtIntersection * (lambda_right - lambda_t);

    //cout << "HELP: " << variationLeft << " " << variationRight << endl;

    setQuadraticErrorVariation(variationLeft + variationRight);
}

void CandidateNode::setQuadraticErrorVariation(const double twoPatternModelG){
    quadraticErrorVariation = twoPatternModelG;
}

list<DendrogramNode*>::iterator CandidateNode::getLeftChild() const{
    return leftChild;
}

list<DendrogramNode*>::iterator CandidateNode::getRightChild() const{
    return rightChild;
}

void CandidateNode::setLambda0(const double param_lambda_0){
    lambda_0 = param_lambda_0;
}

void CandidateNode::setSimilarityShift(const double similarityShift){
    oneMinusSimilarityShift = 1 - similarityShift;
}

const double CandidateNode::getOneMinusSimilarityShift(){
    return oneMinusSimilarityShift;
}

const bool CandidateNode::smallerQuadraticErrorVariation(const CandidateNode* node1, const CandidateNode* node2){
    return node1->quadraticErrorVariation < node2->quadraticErrorVariation;
}
