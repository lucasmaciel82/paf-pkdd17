// Copyright 2007,2008,2009,2010,2011,2012,2013,2014,2015,2016 Loïc Cerf (magicbanana@gmail.com)

// This file is part of paf.

// paf is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation

// paf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with paf; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

#ifndef PARAMETERS_H_
#define PARAMETERS_H_

// Output
/* OUTPUT turns on the output. This option may be disabled to evaluate the performance of paf independently from the writing performances of the disk. */
#define OUTPUT

// Log
/* VERBOSE_PARSER turns on the output (on the standard output) of information when the input data and group files are parsed. */
/* #define VERBOSE_PARSER */

/* DEBUG turns on the output (on the standard output) of information during the agglomeration of the patterns. This option may be enabled by who wishes to understand how this agglomeration is performed on a small number of closed error-tolerant n-sets. */
//#define DEBUG

/* TIME turns on the output (on the standard output) of the running time of paf. */
/* #define TIME */

/* DETAILED_TIME turns on the output (on the standard output) of a more detailed analysis of how the time is spent. It gives (in this order): */
/* - the parsing time */
/* - the agglomeration time */
/* - the selection time */
/* #define DETAILED_TIME */

/* GNUPLOT modifies the outputs of TIME and DETAILED_TIME (in this order). They become tab separated values. */
/* #define GNUPLOT */

#endif /*PARAMETERS_H_*/
