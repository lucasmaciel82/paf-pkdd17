*** Dependencies ***

To run, paf requires the Boost library. Most of GNU/Linux
distributions have it in their repositories. Simply use your package
manager to install it. Notice however that Debian and its derivatives
(including the *buntu) split that library into several packages. If
you run one of them, you must install libboost-dev,
libboost-program-options and libboost-program-options-dev.

To manually install the Boost library (for example if you are running
a proprietary operating system), this Web page may help you:
http://www.boost.org/more/getting_started/index.html


*** Compilation ***

paf was meant to be built with GCC version 4.7 or later. However one
can easily adapt the makefile to use another compiler.

To install paf on a *NIX-like operating system, run (with
administrator privileges):
# make install

If you do not have access to administrator privileges or if you do not
run a *NIX-like operating system, simply run:
$ make
The executable, called paf, is created in the working directory. You
can then move it to a personal directory listed in your PATH variable.


*** Compilation Options ***

Several options can be easily enabled (respectively disabled) in
Parameters.h by uncommenting (respectively commenting) them:

OUTPUT turns on the output. This option may be disabled to evaluate
the performance of paf independently from the writing performances of
the disk.

VERBOSE_PARSER turns on the output (on the standard output) of
information when the input data and group files are parsed.

DEBUG turns on the output (on the standard output) of information
during the agglomeration of the patterns. This option may be enabled
by who wishes to understand how this agglomeration is performed on a
small number of closed error-tolerant n-sets.

TIME turns on the output (on the standard output) of the running time
of paf.

DETAILED_TIME turns on the output (on the standard output) of a more
detailed analysis of how the time is spent. It gives (in this order):
- the parsing time
- the agglomeration time
- the selection time

GNUPLOT modifies the outputs of TIME and DETAILED_TIME (in this
order). They become tab separated values.