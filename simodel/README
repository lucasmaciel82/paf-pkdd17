*** DESCRIPTION ***

simodel compare a set of extracted patterns with a set of
planted patterns, resulting with a value [0,1] which measure the
similarity between those sets.

0 stands for no similarity
1 stands for the tuples covered by the sets are the same

Obs: This measure don't evaluate the redundancy of patterns,


*** RETURN VALUES ***

simodel returns values which can be used when called from a script. They
are conformed to sysexit.h.
* 0 is returned when simodel successfully terminates.
* 64 is returned when simodel was incorrectly called.
* 65 is returned when an input data line is not properly formatted.
* 74 is returned when data could not be read or written on the disk.


*** GENERAL ***

* simodel called with no option or with option --help (or -h) displays a
reminder of the options.


*** INPUT DATA ***

--extracted-n-sets-file:
The set of extracted patterns. The values of each
dimension are separated with ',' (comma) when the dimensions
itself are separated with ' ' (space).

--hidden-n-sets-file
The set of planted patterns. The values of each
dimension are separated with ',' (comma) when the dimensions
itself are separated with ' ' (space).

*** OUTPUT DATA ***

A number v, v \in [0,1], such that 0 stands for no similarity
and 1 for the sets are covering the same tuples.


*** MORE OPTIONS ***

--verbose.
Output the arg max of Jackard measure for each extracted pattern

--average
Output the average of the arg max of Jackard measure for each extracted
pattern instead of the main measure of simodel.

*** BUGS ***

Report bugs to <lcerf@dcc.ufmg.br>.


*** COPYRIGHT ***

Copyright 2007-2016 Loïc Cerf (lcerf@dcc.ufmg.br) This is free
software. You may redistribute copies of it under the terms of the GNU
General Public License <http://www.gnu.org/licenses/gpl.html>. There
is NO WARRANTY, to the extent permitted by law.
